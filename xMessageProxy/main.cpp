/* xMessageProxy
 * Copyright 2015 Keith Kaisershot
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *     (3)The name of the author may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "stdafx.h"

#include "handler.h"
#include "messages.h"

const int SMALL_MSG_DATA_SIZE = 12;

// Global Variables:
HINSTANCE hInst;								// current instance
LPTSTR szWindowClass = HANDLER_NAME L"MessageProxy";			// the main window class name
PROCESS_INFORMATION pi;
xMP::MessageProxy msgProxy;
bool proxyRunning = false;
bool handlerRunning = false;
bool sentHello = false;
bool gotHello = false;
bool deferredPump = false;
xMP::MsgHeader msgHeader;

// Forward declarations of functions included in this code module:
ATOM				RegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK		CloseAppEnum(HWND hwnd, LPARAM lParam);
void				ReceiveHello(const uint32_t size, const void* data);
void				DiscardData(HANDLE hFile, DWORD dwSize, DWORD* dwOutBytesRead);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	RegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	MSG msg;
	DWORD exitCode;
	do
	{
		// Main message loop:
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				// Wait on the handle. If it signals, great. If it times out,
				// then you kill it.
				if (WaitForSingleObject(pi.hProcess, INFINITE) != WAIT_OBJECT_0)
				{
					TerminateProcess(pi.hProcess, 0);
				}
			}
			else
			{
				DispatchMessage(&msg);
			}
		}

		if (handlerRunning)
		{
			if (proxyRunning)
			{
				if (!sentHello)
				{
					sentHello = msgProxy.sendMessage(0, 0, NULL);
				}
				else
				{
					DWORD dwTotalBytesAvail = 0;
					bool success = PeekNamedPipe(xMP::getPipeRead(), NULL, 0, NULL, &dwTotalBytesAvail, NULL) == TRUE;

					while (success && dwTotalBytesAvail > 0)
					{
						DWORD dwBytesRead = 0;

						if (!deferredPump)
						{
							if (dwTotalBytesAvail < sizeof(msgHeader))
							{
								break;
							}

							success = ReadFile(xMP::getPipeRead(), &msgHeader, sizeof(msgHeader), &dwBytesRead, NULL) == TRUE;
							if (!success || dwBytesRead != sizeof(msgHeader))
							{
								break;
							}

							dwTotalBytesAvail -= sizeof(msgHeader);
						}

						deferredPump = dwTotalBytesAvail < msgHeader.size;
						if (deferredPump)
						{
							break;
						}

						if (msgHeader.size == 0)
						{
							msgProxy.receiveMessage(msgHeader.id, msgHeader.size, NULL);
						}
						else if (msgHeader.size <= SMALL_MSG_DATA_SIZE)
						{
							uint8_t msgData[SMALL_MSG_DATA_SIZE];
							success = ReadFile(xMP::getPipeRead(), msgData, msgHeader.size, &dwBytesRead, NULL) == TRUE;
							if (success && dwBytesRead == msgHeader.size)
							{
								msgProxy.receiveMessage(msgHeader.id, msgHeader.size, msgData);
							}
							dwTotalBytesAvail -= dwBytesRead;
						}
						else
						{
							uint8_t* msgData = new uint8_t[msgHeader.size];
							if (msgData == NULL)
							{
								DiscardData(xMP::getPipeRead(), msgHeader.size, &dwBytesRead);
							}
							else
							{
								success = ReadFile(xMP::getPipeRead(), msgData, msgHeader.size, &dwBytesRead, NULL) == TRUE;
								if (success && dwBytesRead == msgHeader.size)
								{
									msgProxy.receiveMessage(msgHeader.id, msgHeader.size, msgData);
								}
								delete[] msgData;
							}
							dwTotalBytesAvail -= dwBytesRead;
						}
					}
				}
			}
			xMP::updateHandler();
		}

		Sleep(1);
	}
	while (GetExitCodeProcess(pi.hProcess, &exitCode) && exitCode == STILL_ACTIVE);

	if (handlerRunning)
	{
		xMP::shutdownHandler();
	}
	if (proxyRunning)
	{
		xMP::shutdown();
	}

	return (int) exitCode;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM RegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= 0;
	wcex.lpfnWndProc	= DefWindowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= NULL;
	wcex.hbrBackground	= NULL;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= NULL;

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindow(szWindowClass, szWindowClass, 0,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

#if !_DEBUG
	if (xMP::verify())
#endif
	{
		proxyRunning = xMP::init();
		if (proxyRunning)
		{
			handlerRunning = xMP::initHandler(msgProxy);
			if (handlerRunning)
			{
				msgProxy.installMessageProc(0, ReceiveHello);
			}
			else
			{
				xMP::shutdownHandler();
			}
		}
		else
		{
			xMP::shutdown();
		}
	}

	const WCHAR* fullPath = xMP::getPeerPath();

	int fullArgc;
	LPWSTR* fullArgv = CommandLineToArgvW(GetCommandLineW(), &fullArgc);

	const int FULL_CMDLINE_SIZE = 32768;
	WCHAR fullCmdLine[FULL_CMDLINE_SIZE];
	int i = 1;
	HRESULT hResult = StringCchCopyW(fullCmdLine, FULL_CMDLINE_SIZE, fullPath);
	PathQuoteSpacesW(fullCmdLine);
	while (hResult == S_OK && i < fullArgc)
	{
		hResult = StringCchCatW(fullCmdLine, FULL_CMDLINE_SIZE, L" ");
		if (hResult == S_OK)
		{
			hResult = StringCchCatW(fullCmdLine, FULL_CMDLINE_SIZE, fullArgv[i++]);
		}
	}

	LocalFree(fullArgv);

	STARTUPINFO si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	if (proxyRunning)
	{
		si.hStdInput = xMP::getPeerPipeRead();
		si.hStdOutput = xMP::getPeerPipeWrite();
		si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
		si.dwFlags |= STARTF_USESTDHANDLES;
	}
	if (!CreateProcessW(fullPath, fullCmdLine, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi))
	{
		return FALSE;
	}

	return TRUE;
}


BOOL CALLBACK CloseAppEnum(HWND hwnd, LPARAM lParam)
{
	PostMessage(hwnd, WM_CLOSE, 0, 0);

	return TRUE;
}

void ReceiveHello(const uint32_t size, const void* data)
{
	if (size == 0 && data == NULL)
	{
		xMP::receiveHello();
		msgProxy.removeMessageProc(0);
	}
}

const int DUMMY_BUFFER_SIZE = 8192;
void DiscardData(HANDLE hFile, DWORD dwSize, DWORD* dwOutBytesRead)
{
	uint8_t dummy[DUMMY_BUFFER_SIZE];
	DWORD dwBytesRead = 0;

	if (dwOutBytesRead)
		*dwOutBytesRead = 0;

	do
	{
		ReadFile(hFile, dummy, DUMMY_BUFFER_SIZE, &dwBytesRead, NULL);
		dwSize -= dwBytesRead;
		if (dwOutBytesRead)
			*dwOutBytesRead += dwBytesRead;
	} while (dwSize > 0 && dwBytesRead > 0);
}
