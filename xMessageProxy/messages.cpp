/* xMessageProxy
 * Copyright 2015 Keith Kaisershot
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *     (3)The name of the author may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "stdafx.h"
#include "messages.h"

#include "handler.h"

namespace xMP
{
#if defined(WIN32)
	// our pipe
	HANDLE hInPipeRead = NULL;
	HANDLE hOutPipeWrite = NULL;
	// peer pipe
	HANDLE hInPipeWrite = NULL;
	HANDLE hOutPipeRead = NULL;
#else
	// our pipe
	int inPipeRead = -1;
	int outPipeWrite = -1;
	// peer pipe
	int inPipeWrite = -1;
	int outPipeRead = -1;
#endif
	bool gotHello = false;

	std::map<uint32_t, MessageProxy::MessageProc> messageProcs;

#if defined(WIN32) 
	bool init()
	{
		bool success = true;

		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(sa);
		sa.lpSecurityDescriptor = NULL;
		sa.bInheritHandle = true;

		if (success)
		{
			success = CreatePipe(&hInPipeRead, &hInPipeWrite, &sa, 0) == TRUE;
		}

		if (success)
		{
			success = CreatePipe(&hOutPipeRead, &hOutPipeWrite, &sa, 0) == TRUE;
		}

		return success;
	}
	HANDLE getPipeRead()
	{
		return hInPipeRead;
	}
	HANDLE getPipeWrite()
	{
		return hOutPipeWrite;
	}
	HANDLE getPeerPipeRead()
	{
		return hOutPipeRead;
	}
	HANDLE getPeerPipeWrite()
	{
		return hInPipeWrite;
	}
	void shutdown()
	{
		CloseHandle(hInPipeRead);
		CloseHandle(hInPipeWrite);
		CloseHandle(hOutPipeRead);
		CloseHandle(hOutPipeWrite);
	}
#else
	bool init()
	{
		myID = id;
		// TODO open the pipe (fd 3)
		return true;
	}
	int getPipeRead()
	{
		return inPipeRead;
	}
	int getPipeWrite()
	{
		return outPipeWrite;
	}
	int getPeerPipeRead()
	{
		return outPipeRead;
	}
	int getPeerPipeWrite()
	{
		return inPipeWrite;
	}
	void shutdown()
	{
		// TODO close the pipe
	}
#endif
	void receiveHello()
	{
		gotHello = true;
	}

	bool MessageProxy::sendMessage(const uint32_t id, const uint32_t size, const void* data)
	{
		if (id != 0 && !gotHello)
		{
			return false;
		}

#if defined(WIN32)
		if (!hOutPipeRead || !hOutPipeWrite)
		{
			return false;
		}

		if (size > 0 && data == NULL)
		{
			return false;
		}

		MsgHeader msgHeader = { 0 };

		msgHeader.id = id;
		msgHeader.size = size;

		DWORD dwBytesWritten = 0;
		bool success = WriteFile(hOutPipeWrite, &msgHeader, sizeof(msgHeader), &dwBytesWritten, NULL) == TRUE;
		
		if (success && dwBytesWritten != sizeof(msgHeader))
		{
			success = false;
		}

		if (success && data != NULL)
		{
			success = WriteFile(hOutPipeWrite, data, msgHeader.size, &dwBytesWritten, NULL) == TRUE;
		}

		if (success && data != NULL && dwBytesWritten != msgHeader.size)
		{
			success = false;
		}

		return success;
#else
		// TODO
		return false;
#endif
	}

	bool MessageProxy::receiveMessage(const uint32_t id, const uint32_t size, const void* data)
	{
		if (id != 0 && !gotHello)
		{
			return false;
		}

		if (size > 0 && data == NULL)
		{
			return false;
		}

		if (messageProcs.find(id) != messageProcs.end())
		{
			messageProcs[id](size, data);
			return true;
		}
		return false;
	}

	void MessageProxy::installMessageProc(uint32_t id, MessageProc proc)
	{
		messageProcs[id] = proc;
	}

	void MessageProxy::removeMessageProc(uint32_t id)
	{
		messageProcs.erase(id);
	}
}
