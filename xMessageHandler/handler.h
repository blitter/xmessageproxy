/* xMessageProxy
 * Copyright 2015 Keith Kaisershot
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *     (3)The name of the author may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HANDLER_H
#define HANDLER_H

#if defined(WIN32)
#define HANDLER_NAME	L"default"
#elif defined(POSIX)
#define HANDLER_NAME	"default"
#else
#define HANDLER_NAME	"default"
#endif

namespace xMP
{
	struct MessageProxy
	{
		typedef void(*MessageProc)(const uint32_t size, const void* data);

		bool sendMessage(const uint32_t id, const uint32_t size, const void* data);
		bool receiveMessage(const uint32_t id, const uint32_t size, const void* data);

		void installMessageProc(const uint32_t id, const MessageProc proc);
		void removeMessageProc(const uint32_t id);
	};

#if defined(WIN32)
	const WCHAR* getPeerPath();
#else
	const char* getPeerPath();
#endif
	bool verify();

	bool initHandler(MessageProxy& proxy);
	void updateHandler();
	void shutdownHandler();
}

#endif
