xMessageProxy
=============

This is a simple IPC-based proxy for forwarding packet-sized commands from one application to another (e.g. server, monitoring tool). Available under the 3-clause BSD license; forks and branches are encouraged. Used with the Steam version of The Journeyman Project: Pegasus Prime.

MessageProxy
------------

The `MessageProxy` class provides an interface to send and receive variable-length messages to and from a properly equipped peer application.

A `MessageProc` procedure takes as input the message's size and, if the size is greater than zero, a pointer to the message data, the contents of which are implementation-defined.

`installMessageProc` assigns a `MessageProc` to the provided command ID which, aside from the reserved "Hello" command ID of zero, are implementation-defined.  
`removeMessageProc` unassigns a previously-defined `MessageProc` from the provided command ID.

`sendMessage` takes as input a message's size, a pointer to the message data (if size is greater than zero), and immediately sends it associated with the provided command ID to the peer application. It returns true if the message is sent.  
`receiveMessage` normally does not need to be called by the message handler, but it works like `sendMessage` in reverse: it looks up the proper `MessageProc` from the provided command ID, and passes the provided size and data pointers to it for handling. It returns true if the message is handled.

xMessageHandler
---------------

The xMessageHandler project contains a do-nothing handler skeleton that should be replaced; it shouldn't be necessary to modify any code in the xMessageProxy project.

The preprocessor macro `HANDLER_NAME` defines the name of the message handler. This may be set to any preferred string.

`getPeerPath` returns an absolute path to the peer application, which is started by the message proxy after initializing the message handler.

`initHandler` is called immediately after successfully creating the two-way pipes for communication with the peer application, providing a reference to a `MessageProxy` object for installing custom `MessageProc`s and sending custom messages. It returns true if initialization is successful.  
`updateHandler` is called after the message proxy has polled and processed any incoming messages.  
`shutdownHandler` is called upon termination of the message proxy, which can occur if a fatal error is encountered or if the peer application terminates.

`verify` returns a `bool` that determines whether or not the message proxy and handler should be initialized. Use this for example to verify the integrity of the peer application before starting your handler. The peer will be launched regardless of the result.

IPC message format
------------------

Messages are sent unencrypted as a stream of bytes by default through stdin/stdout to and from the peer application. The bytestream is prefaced by an eight-byte header, consisting of a 32-bit command ID field, and a 32-bit size field. All complete queued messages are processed at once, serially; if there is a buffer underrun, the message proxy will defer further processing until the remainder of the incomplete message is received. The peer application should do likewise.

As an initial handshake, an eight-byte "Hello" message consisting of all zeroes is sent to the peer after initialization of the message handler. The peer must return its own "Hello" message before establishing any further communication.

The message proxy contains a small optimization that favors four-byte payloads (i.e. a twelve-byte "packet," including the eight-byte header). This is to make the message proxy more efficient as a simple signaling mechanism.

Build Process
-------------

1. Build the xMessageProxy project in the xMessageProxy solution. Dependencies are configured such that xMessageHandler will be built and linked automatically.
2. Modify the peer application to read/write incoming and outgoing messages through stdin/stdout. Build the peer application according to its defined process.

Have fun.

Keith Kaisershot

3-14-17
